import React, {Component} from 'react';
import {Button, Col, Container, Form, FormGroup, Input, Label} from "reactstrap";

import axios from '../../axios-posts'
import withLoader from "../../HOC/withLoader/withLoader";

class AddNewPost extends Component {
    state = {
        description: '',
        title: ''
    };


    valueChange = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value})
    };

    addPost = () => {
        const post = {title: this.state.title, description: this.state.description};
        axios.post('/posts/.json', post).then(() => {
            this.props.history.push('/')
        })
    };

    render() {

        return (
            <Form className="mt-5">
                <Container>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>Title</Label>
                        <Col sm={10}>
                            <Input type="text" name="title" id="exampleEmail" value={this.state.title} onChange={this.valueChange}  />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>description</Label>
                        <Col sm={10}>
                            <Input type="text" name="description" value={this.state.description} onChange={this.valueChange}  />
                        </Col>

                    </FormGroup>
                    <FormGroup row>
                    <Col sm={{size: 10, offset: 2}}>
                        <Button color="primary" onClick={this.addPost}>primary</Button>
                    </Col>
                    </FormGroup>
                </Container>
            </Form>
        );
    }
}

export default  withLoader(AddNewPost, axios);