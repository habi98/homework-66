import React, {Component, Fragment} from 'react';
import axios from '../../axios-posts';
import withLoader from "../../HOC/withLoader/withLoader";
import {Card, CardText, CardTitle, Col, } from "reactstrap";
import Container from "reactstrap/es/Container";

class Posts extends Component {
    state = {
      posts: [],
    };

    componentDidMount() {
        axios.get('/posts/.json').then(response => {
            const posts = Object.keys(response.data).map(key => {
                return {
                    ...response.data[key],
                    id: key
                }
            });
            this.setState({posts: posts})
        })
    }

    render() {
        return (
            <Fragment>
                <Container>
                {this.state.posts.map(post => {
                    return (
                        <Col sm="14" className="mt-4" key={post.id}>
                            <Card body>
                                <CardTitle>{post.title}</CardTitle>
                                <CardText>{post.description}</CardText>
                            </Card>
                        </Col>
                   )
                })}
                </Container>
            </Fragment>

        );
    }
}

export default withLoader(Posts, axios);