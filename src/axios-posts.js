import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://burger-project-habi.firebaseio.com/'
});


export default instance;