import React, {Component, Fragment} from 'react';
import Layout from "./components/Layout/Layout";
import Posts from "./containers/Posts/Posts";
import {BrowserRouter, Route} from "react-router-dom";
import AddNewPost from "./containers/AddNewPost/AddNewPost";

class App extends Component {
  render() {
    return (
        <Fragment>
            <BrowserRouter>
                <Layout>

                    <Route path="/"  exact component={Posts}/>
                    <Route path="/add/post" component={AddNewPost}/>
                </Layout>

            </BrowserRouter>

        </Fragment>

    );
  }
}

export default App;
