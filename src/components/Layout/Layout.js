import React, {Fragment} from 'react';
import Header from "../Navigation/Header/Header";


const Layout = ({children}) => {
    return (
        <Fragment>
            <Header/>
            <div>
                {children}
            </div>
        </Fragment>


    );
};

export default Layout;