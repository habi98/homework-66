import React from 'react';

import NavbarBrand from "reactstrap/es/NavbarBrand";

const Logo = () => (
    <NavbarBrand href="/">reactstrap</NavbarBrand>
);


export default Logo;