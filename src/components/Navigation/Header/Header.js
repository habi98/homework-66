import React from 'react';
import Navbar from "reactstrap/es/Navbar";
import Logo from "../Logo/Logo";
import NavigationItem from "../NavigationItem/NavigationItem";


const Header = () => {
    return (
        <Navbar color="light" light expand="md">
            <Logo/>
          <NavigationItem/>
        </Navbar>
    );
};

export default Header;