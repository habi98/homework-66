import React from 'react';
import {Collapse, Nav, NavItem, NavLink} from "reactstrap";

import {NavLink as RouterNavLink} from "react-router-dom";

const NavigationItem = () => (
    <Collapse  navbar>
        <Nav className="ml-auto" navbar>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/">Post</NavLink>
            </NavItem>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/add/post">Add post</NavLink>
            </NavItem>
        </Nav>
    </Collapse>
);

export default NavigationItem;