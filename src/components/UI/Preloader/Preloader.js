import React from 'react';
import pre from '../../../assets/images/5.gif'
import './Preloader.css'

const Preloader = () => (
    <div className="div-preloader"><img src={pre} alt=""/></div>
);

export default Preloader;