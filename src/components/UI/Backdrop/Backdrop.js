import React from 'react';
import './Backdrop.css'
import Preloader from "../Preloader/Preloader";



const Backdrop = (props) => {
    return (
       props.show ? <div className="Backdrop"><Preloader/></div> : null
    );
};

export default Backdrop;