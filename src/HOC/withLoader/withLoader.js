import React, {Component, Fragment} from 'react';
import Backdrop from "../../components/UI/Backdrop/Backdrop";

const WithLoader = (LoadedComponent, axios) => {
    return  class WithLoaderHoc extends Component{

        constructor(props) {
            super(props);
            this.state ={
                loaded: false
            };
            this.state.interceptorReq = axios.interceptors.request.use(req => {
                this.setState({loaded: true});
                return req
            });
            this.state.interceptorRes = axios.interceptors.response.use(res => {
                this.setState({loaded: false});
                return res
            })
        }

        componentWillUnmount() {
         axios.interceptors.request.eject(this.state.interceptorReq);
         axios.interceptors.response.eject(this.state.interceptorRes)
        }


        render() {
         return(
             <Fragment>
                  <Backdrop show={this.state.loaded}/>
                 <LoadedComponent {...this.props}/>
             </Fragment>
         )
        }
    }

};

export default WithLoader;